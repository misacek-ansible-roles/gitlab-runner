#!/usr/bin/env python

from optparse import OptionParser, OptionGroup
import logging
import os
import sys

GITLAB_URL = os.environ.get('GITLAB_URL', 'https://gitlab.cee.redhat.com')
GITLAB_PRIVATE_TOKEN = os.environ.get('GITLAB_PRIVATE_TOKEN', 'not-realy-a-token')
PYTHON_GITLAB_PATH = (
    "%s/python-gitlab/usr/lib/python2.7/site-packages/" % (
        os.path.dirname(os.path.abspath(__file__))
    )
)

ENABLE_REPOS = os.environ.get('REGISTER_TO_REPOS', 'clusterqe/cqe')
gitlab_repos = ENABLE_REPOS.split(' ')


class GitLabAPIException(Exception):
    ''' Special exception for this script. '''
    pass


def set_logging():
    ''' Return logger object to use here. '''
    logger_name = __name__
    logger = logging.getLogger(logger_name)
    logger.addHandler(logging.StreamHandler())
    return logger
logger = set_logging()


def option_parser():
    '''
        Returns basic parser with --help and --show-all-function parameters.
    '''
    parser = OptionParser(conflict_handler="resolve")

    group1 = OptionGroup(parser, "Mandatory")
    group1.add_option(
        '--runner-name', '-r',
        dest='runner_name', default=None,
        help=('Name of the runner as gitlab sees it.')
    )
    group1.add_option(
        '--add-runner-to-project', '-a',
        dest='projects', action='append', default=None,
        help=(
            'Name of the project in the form PREFIX/NAME. This project will '
            'get runner specified by --runner-name enabled. Can be repeated.'
        )
    )
    group1.add_option(
        '--unregister-runner', '-u',
        dest='unregister_runner', default=False, action="store_true",
        help=('')
    )
    parser.add_option_group(group1)

    group2 = OptionGroup(parser, "Configuration")
    group2.add_option(
        '--gitlab-url',
        dest='GITLAB_URL',
        default=os.environ.get('GITLAB_URL', 'https://gitlab.cee.redhat.com'),
        help=(
            "URL of the gitlab server. Can be specified as GITLAB_URL in env."
        )
    )
    group2.add_option(
        '--gitlab-token',
        dest='GITLAB_PRIVATE_TOKEN',
        default=os.environ.get('GITLAB_PRIVATE_TOKEN', 'THIS-WILL-NOT-WORK'),
        help=(
            "Gitlab api token. In web ui go to Profile settings, access tokens. "
            "You need to provide this or nothing will work."
        )
    )
    group2.add_option(
        '--python-gitlab-path',
        dest='python_gitlab_path',
        default=PYTHON_GITLAB_PATH,
        help=(
            "Path where python-gitlab is installed. Default: %s" % (
                PYTHON_GITLAB_PATH
            )
        )
    )
    group2.add_option(
        '--log-level',
        dest='log_level', default="INFO",
        help=('Loglevel to use. Default: INFO')
    )
    parser.add_option_group(group2)
    return parser


def options_sanity(parser):
    '''
        Checks parser options and if all is well return tuple of (options,
        args).
    '''
    (options, args) = parser.parse_args()
    if options.runner_name is None:
        parser.error('--runner-name must be given')

    if options.projects is None and options.unregister_runner is False:
        parser.error('One of --add-runner-to-project or --unregister-runner must be given.')

    if options.unregister_runner is True and options.projects is not None:
        parser.error(
            '--unregister-runner must not go together with '
            '--add-runner-to-project'
        )

    return (options, args)


def project_name_to_id(gl, name):
    ''' Return id for the project name passed or exception. '''
    projects = gl.projects.list()
    for project in projects:
        # for repo in gitlab_repos:
        if project.path_with_namespace == name:
            return project.id
    else:
        raise GitLabAPIException(
            'No project with name "%s" seems to exist.' % name
        )


def get_runner_id(gl, name):
    ''' Return list of ids or empty list if not found '''
    runners = gl.runners.list()
    rlist = []
    for runner in runners:
        # for repo in gitlab_repos:
        if runner.description == name:
            rlist.append(runner.id)
    return rlist


def main(*args, **kwargs):
    parser = option_parser()
    (options, args) = options_sanity(parser)

    try:
        import optcomplete
        optcomplete.autocomplete(parser)
    except ImportError:
        pass

    log_level = getattr(logging, options.log_level.upper(), None)
    if not isinstance(log_level, int):
        parser.error('Invalid log level: %s' % log_level)
    else:
        logger.setLevel(log_level)

    sys.path.append(options.python_gitlab_path)

    # needs to be imported only after we know python gitlab path from parser
    import gitlab  # pylint: disable=some-message,another-one
    gl = gitlab.Gitlab(
        options.GITLAB_URL,
        private_token=options.GITLAB_PRIVATE_TOKEN,
        api_version='3'
    )
    gl.auth()

    runner_ids = get_runner_id(gl, options.runner_name)
    if len(runner_ids) == 0:
        logger.error('runner %s not found' % options.runner_name )
        sys.exit(1)
    else:
        logger.info('runner %s have id(s) %s' % (options.runner_name, runner_ids))

    # for project in options.projects:
    #    print('%s: %s' % (project, project_name_to_id(gl, project)))

    if options.unregister_runner is False:
	runner_id = runner_ids[0] 
        for enable_in_project in options.projects:
            for project in gl.projects.list():
                # for repo in gitlab_repos:
                if project.path_with_namespace == enable_in_project:
                    logger.info(
                        'Enable runner %s for project %s' % (
                            runner_id, enable_in_project
                        )
                    )
                    try:
                        project.runners.create({'runner_id': runner_id})
                    except gitlab.exceptions.GitlabCreateError as e:
                        created = (
                            '409: Runner was already enabled for this project'
                        )
                        if str(e) == created:
                            logger.warn(e)
                        else:
                            raise(e)
                    logger.info('Runner %s is enabled for project %s' % (
                            runner_id, enable_in_project
                        )
                    )
                    break
            else:
                raise GitLabAPIException(
                    'No project with name "%s" seems to exist.' % (
                        enable_in_project
                    )
                )
    else:
        logger.info('Going to delete runners %s' % runner_ids)
	# TODO: disable first everywhere to get around: registered to more than one project
	for runner_id in runner_ids:
            gl.runners.delete(runner_id)
    return True

if __name__ == '__main__':
    main()
