# Gitlab runners for ClusterQA ci with containers

Shared runners we have cannot be used for docker images creation because the runner needs to have `/var/lib/docker` shared from host machine. This playbook will create our own specific runner on a virtual machine. The runner will be always registered to one project to which `gitlab_runner_registration_token` belongs. It can be automagically enabled in other projects but you need to provide `gitlab_runner_user_token` for that.

File [main.yml](main.yml) contains all the variables that you should want to change. Check it before first run and define correctly at least [`gitlab_runner_user_token`](https://docs.gitlab.com/ce/user/profile/personal_access_tokens.html) and [`gitlab_runner_registration_token`](https://docs.gitlab.com/ce/ci/runners/README.html#registering-a-specific-runner) pass it on command line using `-e gitlab_runner_user_token=<le-token>` and `-e gitlab_runner_registration_token`.

## Possible tags
```
 $ ansible-playbook main.yml --list-tags

 playbook: main.yml

  play #1 (all): Apply common configuration to all nodes.       TAGS: [common]
      TASK TAGS: [common, docker, docker-certificates, docker-fs,
        docker-install, docker-pull, iscsi, rh-certs, rh-repos]

  play #2 (all): Deploy runners.        TAGS: [runners]
      TASK TAGS: [enable-runner, gitlab, gitlab-gpg, gitlab-runner-install,
        install_runner, register-runner, register-runners, runners]
```

## Example usage
Example playbook is available as `tests/main.yml`.

Get a node with big enough iscsi disk from beaker:
```
/usr/bin/generatejob \
    --disk-count=1 \
    --disk-size=10 \
    --nodes=0 \
    --reserve 999 \
    --skip-sts \
    --submit \
    -v 7.4 \
```

You need to export `ANSIBLE_CONFIG` to point to to [ansible.cfg](https://gitlab.cee.redhat.com/clusterqe/ansible-roles/blob/master/ansible.cfg) in the root of the locally cloned [ansible-roles git](https://gitlab.cee.redhat.com/clusterqe/ansible-roles/) so the roles directory can be found:

```
$ export ANSIBLE_CONFIG=<ANSIBLE_ROLES_GIT_ROOT>/ansible.cfg
```

After having exported `ANSIBLE_CONFIG` you can `gitlab-runner` role this way:

You can use it for example this way:
```
$ ansibled-playbook tests/main.yml \
    -e gitlab_runner_registration_token=<token> \
    -e gitlab_runner_user_token=<other token> \
    -e gitlab_enable_runner_in_repos='["username/repo2","username/repo3"]' \
    -i 'virt-432,' \
    -t docker-on-iscsi
```
